const data = [
  {
    nodeId: '1',
    nodeTitle: 'node1',
    nodeChildren: [
      {
        nodeId: '4',
        nodeTitle: 'node4',
        hidden: true,
        nodeChildren: [],
      },
      {
        nodeId: '5',
        nodeTitle: 'node5',
        nodeChildren: [
          {
            nodeId: '6',
            nodeTitle: 'node6',
            nodeChildren: [],
          },
          {
            nodeId: '7',
            nodeTitle: 'node7',
            nodeChildren: [
              {
                nodeId: '8',
                nodeTitle: 'node8',
                nodeChildren: [],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    nodeId: '2',
    nodeTitle: 'node2',
    hidden: true,
    nodeChildren: [],
  },
  {
    nodeId: '3',
    nodeTitle: 'node3',
    nodeChildren: [],
  },
];

const normalize = data =>
  data.filter(isActive).reduce(normalizeToObject(data), {});

const isActive = ({ hidden }) => !hidden;

const normalizeToObject = data => (acc, current) => {
  if (current.nodeChildren.length > 0) {
    const normalizedChildren = normalize(current.nodeChildren);
    const withParentId = Object.keys(normalizedChildren).map(key => ({
      [key]: {
        parentId: current.nodeId,
        ...normalizedChildren[key],
      },
    }));
    acc = Object.assign(acc, ...withParentId);
  }

  acc[current.nodeId] = {
    nodeTitle: current.nodeTitle,
  };

  return acc;
};

console.log(normalize(data));
